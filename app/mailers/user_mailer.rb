class UserMailer < ApplicationMailer
  def email(to:, subject:, body:)
    mail(to: to, subject: subject) do |format|
      format.text { render text: body }
    end
  end
end
