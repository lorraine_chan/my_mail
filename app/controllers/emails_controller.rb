class EmailsController < ApplicationController
  def create
    UserMailer.email(
      to: email_params[:to],
      subject: email_params[:subject],
      body: email_params[:body]
    ).deliver_later

    respond_to do |format|
      format.json { render nothing: true, status: :created }
    end
  end

  private

  def email_params
    params.require(:email).permit(:to, :subject, :body)
  end
end
