require 'rails_helper'

RSpec.describe EmailsController, type: :controller do
  let(:recipient_email) { 'recipient@example.com' }
  let(:the_subject) { 'subject' }
  let(:body) { 'body' }

  describe '#email' do
    it 'sends email with correct arguments' do
      email_params = {
        format: 'json',
        :email => { to: recipient_email, subject: the_subject, body: body }
      }

      post :create, email_params

      expect(response).to have_http_status(:created)

      email = ActionMailer::Base.deliveries.last

      expect(email.to).to eq([recipient_email])
      expect(email.subject).to eq(the_subject)
      expect(email.body.to_s).to eq(body)
    end
  end
end
