class UserMailerPreview < ActionMailer::Preview
  def email
    UserMailer.email('recipient@example.com', 'subject', 'body')
  end
end
