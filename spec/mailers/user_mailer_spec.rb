require 'rails_helper'

RSpec.describe UserMailer do
  describe '.email' do
    let(:recipient_email) { 'recipient@example.com' }
    let(:the_subject) { 'subject' }
    let(:body) { 'body' }

    it 'creates email with correct contents' do
      email = UserMailer.email(
        to: recipient_email,
        subject: the_subject,
        body: body
      )

      expect(email.to).to eq([recipient_email])
      expect(email.subject).to eq(the_subject)
      expect(email.body.to_s).to eq(body)
    end
  end
end
